import styles from './layout.module.scss'

export default function Layout({ children }) {
    return <>
        <div className={styles.container}>
            {children}
        </div>
        <footer className={styles.footer}>
            Created by Hogren for Hogren
        </footer>
    </>
}