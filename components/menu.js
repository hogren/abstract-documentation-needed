import Link from 'next/link';
import styles from './menu.module.scss'

export default function Menu(props) {
    function renderChildren() {
        let count = 0
        return props.items.map(row => {
            return (
                <Link href={row.link} key={count++}>
                    <a className={styles.card}>
                        <h3>{row.title}</h3>
                        <p>{row.description}</p>
                    </a>
                </Link>
            )
        })
    }
    return <div className={styles.grid}>{renderChildren()}</div>
}