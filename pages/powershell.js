import Layout from "../components/layout";
import Link from 'next/link';
import Prism from 'prismjs';
import { useEffect } from 'react';

const powershel = (props) => {

  useEffect(() => {
    Prism.highlightAll();
  }, [])

  let code1 = `param ($envname='TEST')

if ($envname -ne 'TEST' -and $envname -ne 'PROD') {
    Throw "Environment must be TEST or PROD"
}

Write-Output "test"
Exit`;

  return (
    <Layout>
      <main>
        <h1 className="title">
          Powershell Scripting
        </h1>
        <div>
          <h2>Documentation</h2>
          <ul>
            <li>Official site : <a href="https://docs.microsoft.com/fr-fr/powershell/scripting/overview?view=powershell-7.2">Microsoft page</a></li>
          </ul>
        </div>
        <div>
          <p>Exit throwing an error :</p>
          <pre><code className='language-powershell'>{code1}</code></pre>
        </div>
      </main>

      <footer>
        <h2>
          <Link href="/">
            <a>Back to home</a>
          </Link>
        </h2>
      </footer>
    </Layout>);
}

export default powershel;
