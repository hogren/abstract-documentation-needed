import Link from 'next/link'
import Head from 'next/head'
import Layout from '../components/layout'
import Prism from 'prismjs'
import { useEffect } from 'react'

const xml = (props) => {

  useEffect(() => {
    Prism.highlightAll();
  }, [])

  let code1 = `<?xml version="1.0" encoding="utf-8"?>
  <xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:util="http://util.fr/Schema/UTIL">
      <xsd:import namespace="http://util.fr/Schema/UTIL" schemaLocation="util.xsd"/>
      <xsd:annotation>
          <xsd:documentation xml:lang="en">This is an example.</xsd:documentation>
      </xsd:annotation>
      <xsd:element name="propList">
          <xsd:complexType>
              <xsd:sequence>
                  <xsd:element type="xsd:string" name="info1" minOccurs="0" maxOccurs="1"/>
                  <xsd:element type="xsd:string" name="info2" minOccurs="0" maxOccurs="1"/>
              </xsd:sequence>
              <xsd:attribute type="util:specialAttribute" name="specialAttribute" use="required"/> 
          </xsd:complexType>
      </xsd:element>
  </xsd:schema>`
  let code2 = 'var text = "value";'
  return (
    <Layout>
      <main>
        <h1 className="title">
          XML and XSD Schemas
        </h1>
        <div>
          <h2>Tools</h2>
          <ul>
            <li>Test an XML file with XSD (java command) : <a href="https://github.com/amouat/xsd-validator">xsd-validator</a></li>
          </ul>
        </div>
        <div>
          <h2>Documentation</h2>
          <ul>
            <li>Official XSD reference : <a href="https://www.w3.org/TR/xmlschema-0/">W3C Page</a></li>
          </ul>
        </div>
        <div>
          <h2>Basic XSD Scema</h2>
          <p><b>xmlns</b> means XML NameSpace.<br /><code>xmlns:xxx=URI</code> means that xxx is a shortcut to use URI domain. 
          <br/>It’s a good practice to use <b>xsd</b> as standard XMLSchema namespace.
          <br/><code>&lt;xsd:import /&gt;</code> is used to import types of a specific nampespace from a specific file.
          <br/>There is an <code>&lt;xsd:include /&gt;</code> which can be used to include elements and types from another XSD which has no namespace.</p>
          <pre><code className='language-xml'>{code1}</code></pre>
        </div>
      </main>

      <footer>
        <h2>
          <Link href="/">
            <a>Back to home</a>
          </Link>
        </h2>
      </footer>
    </Layout>
  )
}

export default xml;