import Head from 'next/head'
import Link from 'next/link'
import Layout from '../components/layout'
import Menu from '../components/menu'

export default function Home() {
  return (
  <Layout>
    <Head>
      <title>ADN - Abstract Documentation Needed !</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <main>
      <h1 className="superTitle">
        Welcome to <a>ADN</a>
      </h1>

      <p className="description">
        Abstract Documentation Needed
      </p>

      <Menu items={[
        {
          'title': 'XML / XSD Schema',
          'description': 'Tips and examples.',
          'link': '/xml-xsd'
        },
        {
          'title': 'Powershell',
          'description': 'Tips and examples.',
          'link': '/powershell'
        },
        {
          'title': 'NextJS',
          'description': 'Tips and examples.',
          'link': '/nextjs'
        }
      ]} />
    </main>
  </Layout>
  )
}
