import '../styles/global.scss'
import "prismjs/themes/prism-tomorrow.css";

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
